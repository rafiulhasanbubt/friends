//
//  DataModel.swift
//  Friends
//
//  Created by rafiul hasan on 5/21/21.
//

import Foundation

//https://randomuser.me/api/?results=10

class DataModel {    
    func loadFriends(completion: @escaping( (Array<Result>) -> Void)) {
        guard let url = URL(string: "https://randomuser.me/api/?results=10") else { return }
        
        let dataTask = URLSession.shared.dataTask(with: url) { (data, _, _) in
            guard let data = data else {
                completion([])
                return
            }
            
            if let friendResponse = try? JSONDecoder().decode(FriendList.self, from: data) {
                completion(friendResponse.results)
            }
        }
        dataTask.resume()
    }
}

// MARK: - FriendList
struct FriendList: Codable {
    let results: [Result]
}

// MARK: - Result
struct Result: Codable {
    let name: Name
    let location: Location
    let email: String
    let phone, cell: String
    let picture: Picture
    let id: ID
}

// MARK: - ID
struct ID: Codable {
    let name: String
    let value: String?
}

// MARK: - Location
struct Location: Codable {
    let street: Street
    let city, state, country: String
}

// MARK: - Street
struct Street: Codable {
    let number: Int
    let name: String
}

// MARK: - Name
struct Name: Codable {
    let title, first, last: String
}

// MARK: - Picture
struct Picture: Codable {
    let large, medium, thumbnail: String
}
