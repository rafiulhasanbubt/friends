//
//  ImageManager.swift
//  Friends
//
//  Created by rafiul hasan on 5/21/21.
//

import Foundation
import UIKit

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}

class CustomImageView: UIImageView {
    func loadImage(from url: URL) {
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data, let newImage = UIImage(data: data) else { return }
            DispatchQueue.main.async {
                self.image = newImage
            }
        }
        task.resume()
    }
}
