//
//  FriendCollectionViewCell.swift
//  Friends
//
//  Created by rafiul hasan on 5/21/21.
//

import UIKit

class FriendCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
    func setupUI(friend: Result) {
        let url = URL(string: friend.picture.large)
        imageView.load(url: url!)
        fullNameLabel.text = "\(friend.name.first) \(friend.name.last)"
        countryLabel.text = friend.location.country
    }
}
