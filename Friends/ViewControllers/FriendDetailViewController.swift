//
//  FriendDetailViewController.swift
//  Friends
//
//  Created by rafiul hasan on 5/21/21.
//

import UIKit
import MessageUI

class FriendDetailViewController: UIViewController {
    
    static let id = "FriendDetailViewController"
    
    @IBOutlet var profileImageV: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var cityLabel: UILabel!
    @IBOutlet var stateLabel: UILabel!
    @IBOutlet var countryLabel: UILabel!
    @IBOutlet var mobileNoLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    
    var user: Result!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "\(user.name.first) \(user.name.last)"
        let fullName = user!.name.first + " " + user!.name.last
        let address = "\(user.location.street.number) \(user.location.street.name)"
        let url = URL(string: user.picture.large)
        
        profileImageV.load(url: url!)
        nameLabel.text = fullName
        addressLabel.text = address
        cityLabel.text = user?.location.city
        stateLabel.text = user?.location.state
        countryLabel.text = user?.location.country
        mobileNoLabel.text = user?.phone
        emailLabel.text = user?.email
        
        // MARK: make email label clickable with an action
        let tap = UITapGestureRecognizer(target: self, action: #selector(emailLabelPressed))
        emailLabel.isUserInteractionEnabled = true
        emailLabel.addGestureRecognizer(tap)
    }
    
    @objc func emailLabelPressed(sender:UITapGestureRecognizer) {
        print("tap working")
        if let email = emailLabel.text {
            openEmail(email)
        }
    }
    
}

extension FriendDetailViewController: MFMailComposeViewControllerDelegate {
    func openEmail(_ emailAddress: String) {
        // If user has not setup any email account in the iOS Mail app
        if MFMailComposeViewController.canSendMail() == false {
            print("Mail services are not available")
            if let url = URL(string: "mailto:" + emailAddress) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    UIApplication.shared.openURL(url)
                }
            }
            return
        } else {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            composeVC.setToRecipients([emailAddress])
            composeVC.setSubject("")
            composeVC.setMessageBody("", isHTML: false)
            
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
        }
        
        // MARK: MailComposeViewControllerDelegate
        func mailComposeController(_: MFMailComposeViewController, didFinishWith: MFMailComposeResult, error: Error?) {
            // Dismiss the mail compose view controller.
            self.dismiss(animated: true, completion: nil)
        }
    }
}
