//
//  ViewController.swift
//  Friends
//
//  Created by rafiul hasan on 5/21/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var friendCollectionView: UICollectionView!
    
    var dataModel: DataModel = DataModel()
    var userList : [Result] = [Result]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        friendCollectionView.delegate = self
        friendCollectionView.dataSource = self
        friendCollectionView.collectionViewLayout = UICollectionViewFlowLayout()
        // Do any additional setup after loading the view.
        
        dataModel.loadFriends { friend in
            DispatchQueue.main.async {
                self.userList = friend
                self.friendCollectionView.reloadData()
            }
        }
    }

}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    //MARK: for datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return userList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FriendCollectionViewCell", for: indexPath) as! FriendCollectionViewCell
        cell.layer.cornerRadius = 16
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.setupUI(friend: userList[indexPath.row])
        return cell
    }
    
    //MARK: Delegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "FriendDetailViewController") as! FriendDetailViewController
        vc.user = userList[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: Flowlayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let bounds = collectionView.bounds
        return CGSize(width: bounds.width/2 - 10, height: bounds.width/2 + 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
